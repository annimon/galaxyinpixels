package com.annimon.gipgame;

import java.util.Random;
import android.util.DisplayMetrics;

public class Util {
    
    private static Random rnd = new Random();
    
    public static int rand(int to) {
        return rnd.nextInt(to);
    }
    
    public static int rand(int from, int to) {
        return rnd.nextInt(to - from) + from;
    }
    
    public static float rand(float from, float to) {
        return rnd.nextFloat() * (to - from) + from;
    }
    
    public static int randomColor(int from, int to) {
        if (from > 230) from = 230;
        if (to > 255) to = 255;
        
        int red = rand(from, to);
        int green = rand(from, to);
        int blue = rand(from, to);
        
        return 0xFF000000 | (red << 16) | (green << 8) | blue;
    }
    
    public static int randomSpaceColor(int to) {
        if (to > 240) to = 240;
        
        int red = rand(to / 2);
        int green = rand(red / 2, to / 2);
        int blue = rand(red, to);
        
        return 0xFF000000 | (red << 16) | (green << 8) | blue;
    }
    
    /**
     * 
     * @param color1
     * @param color2
     * @param weight - percent of weight (0..1)
     * @return
     */
    public static int getSmoothColor(int color1, int color2, float weight) {
        int r1 = (color1 >> 16) & 0xFF;
        int g1 = (color1 >> 8) & 0xFF;
        int b1 = color1 & 0xFF;
        
        int r2 = (color2 >> 16) & 0xFF;
        int g2 = (color2 >> 8) & 0xFF;
        int b2 = color2 & 0xFF;
        
        int red = r1 + (int) ((r2 - r1) * weight);
        int green = g1 + (int) ((g2 - g1) * weight);
        int blue = b1 + (int) ((b2 - b1) * weight);
        
        return 0xFF000000 | (red << 16) | (green << 8) | blue;
    }
    
    public static float convertDpToPixel(float dp, DisplayMetrics metrics) {
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

}
