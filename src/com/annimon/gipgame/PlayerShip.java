package com.annimon.gipgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.view.View;


public class PlayerShip {
    
    private static final int WIDTH = 5, HEIGHT = 4;
    private static int[] SHIP_COLORS = {
        Color.WHITE,
        Color.GREEN,
        Color.BLUE,
        Color.CYAN,
        Color.YELLOW,
        Color.DKGRAY,
        Color.MAGENTA,
        Color.RED,
        Color.BLACK,
        Color.TRANSPARENT
    };
    
    private int SHIP_WIDTH, SHIP_HEIGHT;
    private int TEXT_SIZE;
    
    private int x, y;
    private int score;
    private Bitmap shipBitmap;
    
    private View view;
    
    public PlayerShip(View view) {
        this.view = view;
        score = 0;
    }
    
    public void init(DisplayMetrics metrics) {
        final int w = view.getWidth();
        final int h = view.getHeight();
        
        SHIP_WIDTH  = (int) Util.convertDpToPixel(w / 16, metrics);
        SHIP_HEIGHT = (int) Util.convertDpToPixel(h / 20, metrics);
        TEXT_SIZE = (int) Util.convertDpToPixel(12, metrics);
        
        createPlayerShip(SHIP_COLORS[0]);
        
        x = (w - SHIP_WIDTH) / 2;
        y = h - SHIP_HEIGHT * 2;
    }
    
    public int getScore() {
        return score;
    }
    
    public boolean gameIsFinished() {
        return ( score > (SHIP_COLORS.length * 20) );
    }

    public void draw(Canvas canvas, Paint paint) {
        // When game is finished we don't need to draw ship.
        if (gameIsFinished()) {
            drawText("Game is finished", canvas, paint, true);
            return;
        }
        // Draw ship.
        canvas.drawBitmap(shipBitmap, x, y, paint);
        // Draw score.
        drawText(String.valueOf(score), canvas, paint, false);
    }
    
    public void updateX(int value) {
        x -= value;
        
        if (x < 0) x = 0;
        else if (x > ( view.getWidth() - SHIP_WIDTH )) {
            x = view.getWidth() - SHIP_WIDTH;
        }
    }
    
    public boolean isCollide(RectF rectangle) {
        return (rectangle.intersects(x, y, x + SHIP_WIDTH, y + SHIP_HEIGHT));
    }
    
    public void changeScore(int value) {
        if (gameIsFinished()) return;
        
        score += value;
        if (score < 0) score = 0;
        else if (score % 20 == 0) {
            // Change color of the ship by score level.
            int shipLevel = Math.min(score / 20, SHIP_COLORS.length - 1);
            createPlayerShip(SHIP_COLORS[shipLevel]);
        }
    }
    
    private void createPlayerShip(int color) {
        Bitmap bmp = Bitmap.createBitmap(WIDTH, HEIGHT, Bitmap.Config.ARGB_8888);
        bmp.setPixel(2, 0, color);
        bmp.setPixel(1, 1, color); bmp.setPixel(3, 1, color);
        bmp.setPixel(1, 2, color); bmp.setPixel(3, 2, color);
        bmp.setPixel(0, 3, color); bmp.setPixel(4, 3, color);
        // Stretch bitmap to calculated size.
        shipBitmap = Bitmap.createScaledBitmap(bmp, SHIP_WIDTH, SHIP_HEIGHT, false);
    }
    
    private void drawText(String text, Canvas canvas, Paint paint, boolean vertCenter) {
        paint.setTextSize(TEXT_SIZE);
        paint.setTextScaleX(2);
        paint.setColor(Color.YELLOW);
        paint.setTextAlign(Paint.Align.CENTER);
        Rect textBounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), textBounds);
        int y = textBounds.height() * 2;
        if (vertCenter) y += view.getHeight() / 2;
        canvas.drawText(text, view.getWidth() / 2, y, paint);
    }
}
