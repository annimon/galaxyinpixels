package com.annimon.gipgame;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.view.View;

public class Bonus {
    
    private static int BONUS_SIZE = 16;
    private static int HORIZ_RANGE = 320;
    private static int HEIGHT = 480;
    
    public static void initBonusSize(View view, DisplayMetrics metrics) {
        final int width = view.getWidth();
        HEIGHT = view.getHeight();
        
        BONUS_SIZE  = (int) Util.convertDpToPixel(width / 30, metrics);
        HORIZ_RANGE = width - BONUS_SIZE;
    }
    
    
    private int color;
    private float speed;
    private RectF bonusRect;
    
    public Bonus() {
        bonusRect = new RectF(0, 0, BONUS_SIZE, BONUS_SIZE);
        initNewPosition();
    }

    public void draw(Canvas canvas, Paint paint) {
        paint.setColor(color);
        paint.setAlpha(200);
        canvas.drawRect(bonusRect, paint);
        paint.setAlpha(255);
    }
    
    public void update(PlayerShip player) {
        bonusRect.offset(0, speed);
        if (bonusRect.top > HEIGHT) {
            initNewPosition();
            player.changeScore(-1);
        } else {
            if (Util.rand(50) == 5) changeSpeed();
        }
        if (player.isCollide(bonusRect)) {
            initNewPosition();
            player.changeScore(1);
        }
    }
    
    private void initNewPosition() {
        color = Util.randomColor(128, 255);
        changeSpeed();
        
        int x = Util.rand(HORIZ_RANGE);
        int y = - BONUS_SIZE - Util.rand(HEIGHT);
        bonusRect.offsetTo(x, y);
    }
    
    private void changeSpeed() {
        speed = Util.rand(0.2f, 4f);
    }
}
