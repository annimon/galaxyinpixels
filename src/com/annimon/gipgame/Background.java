package com.annimon.gipgame;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Smoothly change background color.
 * @author aNNiMON
 */
public class Background {
    
    private static final int MAX_SPACE_COLOR = 70;
    private static final int TICKS_TO_SHIFT_COLOR = 1000;
    private static final int COLORS_IN_ARRAY = 2;
    
    private int tickCounter;
    private int currentColor;
    private int[] colors;
    
    public Background() {
        tickCounter = 0;
        
        colors = new int[COLORS_IN_ARRAY];
        for (int i = 0; i < COLORS_IN_ARRAY; i++) {
            colors[i] = Util.randomSpaceColor(MAX_SPACE_COLOR);
        }
        currentColor = colors[0];
    }

    public void draw(Canvas canvas, Paint paint) {
        // Fill screen by bg color.
        canvas.drawColor(currentColor);
    }
    
    public void update() {
        tickCounter++;
        
        currentColor = Util.getSmoothColor(colors[0], colors[1],
                (float) tickCounter / TICKS_TO_SHIFT_COLOR);
        
        // Remove last and add new background color.
        if (tickCounter >= TICKS_TO_SHIFT_COLOR) {
            tickCounter = 0;
            
            System.arraycopy(colors, 1, colors, 0, COLORS_IN_ARRAY - 1);
            colors[COLORS_IN_ARRAY - 1] = Util.randomSpaceColor(MAX_SPACE_COLOR);
        }
    }
    
    
}
