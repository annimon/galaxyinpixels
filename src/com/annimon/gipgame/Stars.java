package com.annimon.gipgame;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * Stars.
 * @author aNNiMON
 */
public class Stars {
    
    private static final int STARS_COUNT = 200;
    
    private static int width, height;
    
    public static void initSize(View view) {
        width = view.getWidth();
        height = view.getHeight();
    }
    
    private float[] x, y, dirY;
    private int[] color;
    
    public Stars() {
        init();
    }

    public void draw(Canvas canvas, Paint paint) {
        for (int i = 0; i < STARS_COUNT; i++) {
            paint.setColor(color[i]);
            canvas.drawPoint(x[i], y[i], paint);
        }
    }
    
    public void update() {
        for (int i = 0; i < STARS_COUNT; i++) {
            y[i] += dirY[i];
            if (y[i] >= height) {
                initStar(i);
            }
        }
    }
    
    private void init() {
        x = new float[STARS_COUNT];
        y = new float[STARS_COUNT];
        dirY = new float[STARS_COUNT];
        color = new int[STARS_COUNT];
        for (int i = 0; i < STARS_COUNT; i++) {
            initStar(i);
        }
    }

    private void initStar(int i) {
        x[i] = Util.rand(width);
        y[i] = Util.rand(height);
        dirY[i] = Util.rand(0.5f, 5f);
        color[i] = Util.randomColor(80, 255);
    }
}
