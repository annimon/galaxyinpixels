package com.annimon.gipgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;

public class GameView extends View implements SensorEventListener, Runnable {
    
    private static final int PLANET_COUNT = 2;
    private static final int BONUS_COUNT = 10;
    
    private Background background;
    private Stars stars;
    private PlayerShip player;
    private Planet[] planets;
    private Bonus[] bonus;
    
    private Paint paint;
    private SensorManager sensorManager;
    
    private Thread thread;
    
    public GameView(Context context) {
        super(context);
        initView(context);
    }
    
    public GameView(Context context, AttributeSet attributes) {
        super(context, attributes);
        initView(context);
    }
    
    public GameView(Context context, AttributeSet attributes, int defStyle) {
        super(context, attributes, defStyle);
        initView(context);
    }
    
    private void initView(Context context) {
        background = new Background();
        player = new PlayerShip(this);
        planets = new Planet[PLANET_COUNT];
        /*for (int i = 0; i < PLANET_COUNT; i++) {
            planets[i] = new Planet();
        }*/
        
        bonus = new Bonus[BONUS_COUNT];
        for (int i = 0; i < BONUS_COUNT; i++) {
            bonus[i] = new Bonus();
        }
        
        paint = new Paint();
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        
        thread = new Thread(this);
        thread.start();
    }
    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (player == null) player = new PlayerShip(this);
        player.init(getResources().getDisplayMetrics());
        
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        Planet.initSize(this, metrics);
        for (int i = 0; i < PLANET_COUNT; i++) {
            planets[i] = new Planet(i);
        }
        
        Bonus.initBonusSize(this, metrics);
        for (int i = 0; i < BONUS_COUNT; i++) {
            bonus[i] = new Bonus();
        }
        
        Stars.initSize(this);
        stars = new Stars();
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        
        background.draw(canvas, paint);
        stars.draw(canvas, paint);
        for (int i = 0; i < PLANET_COUNT; i++) {
            planets[i].draw(canvas, paint);
        }
        player.draw(canvas, paint);
        if (!player.gameIsFinished()) {
            for (int i = 0; i < BONUS_COUNT; i++) {
                bonus[i].draw(canvas, paint);
            }
        }
        // Draw FPS.
        //paint.setColor(Color.WHITE);
        //canvas.drawText(Fps.getFpsAsString(), 10, 10, paint);
    }
    
    private void update() {
        background.update();
        if (stars != null) stars.update();
        for (int i = 0; i < PLANET_COUNT; i++) {
            if (planets[i] == null) break;
            planets[i].update();
        }
        
        if (player.gameIsFinished()) return;
        
        for (int i = 0; i < BONUS_COUNT; i++) {
            bonus[i].update(player);
        }
    }
    
    @Override
    public void run() {
        while(Thread.currentThread() == thread) {
            Fps.startMeasuringDelay();
            update();
            postInvalidate();
            try {
                Thread.sleep(Fps.getDelay());
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Quit application if user press the back key.
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            background = null;
            player = null;
            bonus = null;
            unregisterSensorListener();
        }

        return true;
    }
    
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float delta = 0.8f;
        float xChange = delta * event.values[0];
        // Control player ship only by x-axis.
        player.updateX((int)xChange);
    }
    
    public void registerSensorListener() {
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }
    
    public void unregisterSensorListener() {
        sensorManager.unregisterListener(this);
    }

}
