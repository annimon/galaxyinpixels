package com.annimon.gipgame;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader.TileMode;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Planet.
 * @author aNNiMON
 */
public class Planet {
    
    private static int width, height;
    private static float dpFactor;
    
    public static void initSize(View view, DisplayMetrics metrics) {
        width = view.getWidth();
        height = view.getHeight();
        
        dpFactor = Util.convertDpToPixel(100, metrics) / 100;
    }
    
    private int color1, color2;
    private int radius;
    private float x, y, dirX, dirY;
    
    private Bitmap bitmap;
    
    public Planet(int id) {
        initStar();
        if (id != 0) {
            y -= height * id;
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        canvas.drawBitmap(bitmap, x, y, paint);
    }
    
    public void update() {
        x += dirX;
        y += dirY;
        
        if (y >= height) {
            initStar();
        }
    }
    
    private void initStar() {
        radius = (int) (Util.rand(5, width / 3) * dpFactor);
        x = Util.rand(width);
        y = - (radius * 2) - Util.rand(height);
        dirX = Util.rand(-0.7f, 0.7f);
        dirY = Util.rand(0.4f, 5f);
        
        color1 = Util.randomColor(120, 255);
        color2 = Util.randomColor(0, 50) | (Util.rand(128) << 24);
        
        makeBitmap();
    }
    
    private void makeBitmap() {
        RadialGradient gradient = new RadialGradient(radius, radius, radius,
                color1, color2, TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setShader(gradient);

        bitmap = Bitmap.createBitmap(radius * 2, radius * 2, Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        c.drawCircle(radius, radius, radius, paint);
    }
}
