package com.annimon.gipgame;

import android.os.Bundle;
import android.view.WindowManager;
import android.app.Activity;

public class GipGameActivity extends Activity {
    
    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Set backlight always on. 
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                             WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        gameView = new GameView(this);
        gameView.setFocusable(true);

        setContentView(gameView);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        gameView.registerSensorListener();
    }

    @Override
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        gameView.unregisterSensorListener();
    }

}
